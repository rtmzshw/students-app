package com.example.studnetapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static ArrayList<StudentData> usersList;
    private RecyclerView recyclerView;

    private BroadcastReceiver addStudent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                Bundle bundle1 = intent.getBundleExtra("STUDENT_DETAILS");
                StudentData student = new StudentData(bundle1.getString("name"), bundle1.getString("id"), bundle1.getString("phone"), bundle1.getString("address"), bundle1.getString("pictureUlr"), bundle1.getBoolean("checkbox"));
                addStudentToList(student);
            }
        }
    };

    private BroadcastReceiver updateStudent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                Bundle bundle1 = intent.getBundleExtra("OLD_STUDENT_DETAILS");
                StudentData oldStudent = new StudentData(bundle1.getString("name"), bundle1.getString("id"), bundle1.getString("phone"), bundle1.getString("address"), bundle1.getString("pictureUlr"), bundle1.getBoolean("checkbox"));
                Bundle bundle2 = intent.getBundleExtra("NEW_STUDENT_DETAILS");
                StudentData newStudent = new StudentData(bundle2.getString("name"), bundle2.getString("id"), bundle2.getString("phone"), bundle2.getString("address"), bundle2.getString("pictureUlr"), bundle2.getBoolean("checkbox"));
                updateStudent(oldStudent, newStudent);
            }
        }
    };

    private BroadcastReceiver deleteStudent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                Bundle bundle1 = intent.getBundleExtra("STUDENT_DETAILS");
                StudentData student = new StudentData(bundle1.getString("name"), bundle1.getString("id"), bundle1.getString("phone"), bundle1.getString("address"), bundle1.getString("pictureUlr"), bundle1.getBoolean("checkbox"));
                deleteStudent(student);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.studentRecycleView);
        if (usersList == null) {
            usersList = new ArrayList<>();
            setStudentData();
        }
        setAdapter();
        LocalBroadcastManager.getInstance(this).registerReceiver(updateStudent, new IntentFilter("update-student"));
        LocalBroadcastManager.getInstance(this).registerReceiver(addStudent, new IntentFilter("add-student"));
        LocalBroadcastManager.getInstance(this).registerReceiver(deleteStudent, new IntentFilter("delete-student"));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(updateStudent);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(addStudent);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(deleteStudent);
        super.onDestroy();
    }

    public static void addStudentToList(StudentData student) {
        usersList.add(student);
    }

    public static void updateStudent(StudentData preStudent, StudentData newStudent) {
        for (StudentData studentData : usersList) {
            if (studentData.getId().equals(preStudent.getId())) {
                deleteStudent(preStudent);
                addStudentToList(newStudent);
                break;
            }
        }
    }

    public static void deleteStudent(StudentData student) {
        for (int i = 0; i < usersList.size(); i++) {
            if (student.getId().equals(usersList.get(i).getId())) {
                usersList.remove(i);
                break;
            }
        }
    }

    public void goToAddStudentPage(View view) {
        Intent intent = new Intent(this, addStudent.class);
        startActivity(intent);
    }

    private void setStudentData() {
        usersList.add(new StudentData("Rotem", "1", "0505878758", "haifa", "@mipmap/person", true));
        usersList.add(new StudentData("Oren", "2", "05277757774", "tel aviv","@mipmap/person", false));
        usersList.add(new StudentData("Yogev", "4", "05277757774", "gedera","@mipmap/person", true));
        usersList.add(new StudentData("Adi", "5", "05277757774", "yafo","@mipmap/person", false));
        usersList.add(new StudentData("Shaked", "6", "05277757774", "yafo","@mipmap/person", true));
        usersList.add(new StudentData("Maor", "7", "05277757774", "yafo","@mipmap/person", false));
        usersList.add(new StudentData("Liat", "8", "05277757774", "yafo","@mipmap/person", false));
        usersList.add(new StudentData("EZ", "9", "05277757774", "yafo","@mipmap/person", false));
        usersList.add(new StudentData("Josef", "10", "05277757774", "yafo","@mipmap/person", false));
        usersList.add(new StudentData("Amnon", "11", "05277757774", "yafo","@mipmap/person", false));
        usersList.add(new StudentData("Nir", "12", "05277757774", "yafo","@mipmap/person", false));

    }

    private void setAdapter() {
        StudentListAdapter adapter = new StudentListAdapter(usersList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}

class StudentData {
    String name;
    String pictureUlr;
    String address;
    String phone;
    String id;
    boolean checkbox;

    public StudentData(String name, String id, String phone, String address, String pictureUlr, boolean checkbox) {
        this.name = name;
        this.pictureUlr = pictureUlr;
        this.id = id;
        this.phone = phone;
        this.address = address;
        this.checkbox = checkbox;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPictureUlr() {
        return pictureUlr;
    }

    public void setPictureUlr(String pictureUlr) {
        this.pictureUlr = pictureUlr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }

    public String getAddress() {return address;}

    public void setAddress(String address) {this.address = address;}

    public String getPhone() {return phone;}

    public void setPhone(String phone) {this.phone = phone;}
}


