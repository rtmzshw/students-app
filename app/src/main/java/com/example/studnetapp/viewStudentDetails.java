package com.example.studnetapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class viewStudentDetails extends AppCompatActivity {
StudentData studentData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student_details);

        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getBundleExtra("STUDENT_DETAILS");
            studentData = new StudentData(bundle.getString("name"),bundle.getString("id"),bundle.getString("phone"), bundle.getString("address"),bundle.getString("pictureUlr"), bundle.getBoolean("checkbox"));
            ((TextView)findViewById(R.id.student_name)).setText(studentData.name);
            ((TextView)findViewById(R.id.student_id)).setText(studentData.id);
            ((TextView)findViewById(R.id.view_student_phone)).setText(studentData.phone);
            ((TextView)findViewById(R.id.student_address)).setText(studentData.address);
            ((CheckBox)findViewById(R.id.view_checkBox)).setChecked(studentData.checkbox);
        }
    }

    public void goToStudentListPage(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void goToEditPage(View view) {
        Intent intent = new Intent(this, editStudent.class);
        Bundle bundle = new Bundle();
        bundle.putString("name", studentData.name);
        bundle.putString("id", studentData.id);
        bundle.putString("pictureUlr", studentData.pictureUlr);
        bundle.putString("phone", studentData.phone);
        bundle.putString("address", studentData.address);
        bundle.putBoolean("checkbox", studentData.checkbox);
        intent.putExtra("STUDENT_DETAILS", bundle);
        startActivity(intent);
    }
}