package com.example.studnetapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import android.os.Bundle;
import android.util.Log;

public class addStudent extends AppCompatActivity {
    StudentData studentData = new StudentData("", "", "0", "", "", false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
    }

    public void goToStudentListPage(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onSave(View view) {
        studentData.setName(((EditText)findViewById(R.id.editName)).getText().toString());
        studentData.setId(((EditText)findViewById(R.id.editId)).getText().toString());
        studentData.setPhone(((EditText)findViewById(R.id.editPhone)).getText().toString());
        studentData.setAddress(((EditText)findViewById(R.id.editAddress)).getText().toString());
        studentData.setCheckbox(((CheckBox)(findViewById(R.id.checkBox))).isChecked());
        Intent intent = new Intent("add-student");
        Bundle bundle = new Bundle();
        bundle.putString("name", studentData.name);
        bundle.putString("id", studentData.id);
        bundle.putString("pictureUlr", studentData.pictureUlr);
        bundle.putString("phone", studentData.phone);
        bundle.putString("address", studentData.address);
        bundle.putBoolean("checkbox", studentData.checkbox);
        intent.putExtra("STUDENT_DETAILS", bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        goToStudentListPage(view);
    }

    public void onCancel(View view) {
        studentData = new StudentData("", "", "0", "", "", false);

        ((EditText)findViewById(R.id.editName)).setText("");
        ((EditText)findViewById(R.id.editId)).setText("");
        ((EditText)findViewById(R.id.editPhone)).setText("");
        ((EditText)findViewById(R.id.editAddress)).setText("");
        ((CheckBox)(findViewById(R.id.checkBox))).setChecked(false);
    }
}