package com.example.studnetapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class editStudent extends AppCompatActivity {
    StudentData studentData;
    StudentData editedStudentData = new StudentData("", "", "", "", "", false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);

        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getBundleExtra("STUDENT_DETAILS");
            studentData = new StudentData(bundle.getString("name"),bundle.getString("id"),bundle.getString("phone"), bundle.getString("address"),bundle.getString("pictureUlr"), bundle.getBoolean("checkbox"));
            ((TextView)findViewById(R.id.editName)).setText(studentData.name);
            ((TextView)findViewById(R.id.editId)).setText(studentData.id);
            ((TextView)findViewById(R.id.editPhone)).setText(studentData.phone);
            ((TextView)findViewById(R.id.editAddress)).setText(studentData.address);
            ((CheckBox)findViewById(R.id.checkBox)).setChecked(studentData.checkbox);
        }
    }

    public void goToStudentListPage(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void updateStudent() {
        Intent intent = new Intent("update-student");
        Bundle bundle = new Bundle();
        bundle.putString("name", studentData.name);
        bundle.putString("id", studentData.id);
        bundle.putString("pictureUlr", studentData.pictureUlr);
        bundle.putString("phone", studentData.phone);
        bundle.putString("address", studentData.address);
        bundle.putBoolean("checkbox", studentData.checkbox);
        intent.putExtra("OLD_STUDENT_DETAILS", bundle);
        Bundle bundle2 = new Bundle();
        bundle2.putString("name", editedStudentData.name);
        bundle2.putString("id", editedStudentData.id);
        bundle2.putString("pictureUlr", editedStudentData.pictureUlr);
        bundle2.putString("phone", editedStudentData.phone);
        bundle2.putString("address", editedStudentData.address);
        bundle2.putBoolean("checkbox", editedStudentData.checkbox);
        intent.putExtra("NEW_STUDENT_DETAILS", bundle2);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void deleteStudent() {
        Intent intent = new Intent("delete-student");
        Bundle bundle = new Bundle();
        bundle.putString("name", studentData.name);
        bundle.putString("id", studentData.id);
        bundle.putString("pictureUlr", studentData.pictureUlr);
        bundle.putString("phone", studentData.phone);
        bundle.putString("address", studentData.address);
        bundle.putBoolean("checkbox", studentData.checkbox);
        intent.putExtra("STUDENT_DETAILS", bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void onSave(View view) {
        editedStudentData.setName(((EditText)findViewById(R.id.editName)).getText().toString());
        editedStudentData.setId(((EditText)findViewById(R.id.editId)).getText().toString());
        editedStudentData.setPhone(((EditText)findViewById(R.id.editPhone)).getText().toString());
        editedStudentData.setAddress(((EditText)findViewById(R.id.editAddress)).getText().toString());
        editedStudentData.setCheckbox(((CheckBox)(findViewById(R.id.checkBox))).isChecked());
        updateStudent();
        goToStudentListPage(view);
    }

    public void onDelete(View view) {
        deleteStudent();
        goToStudentListPage(view);
    }
}