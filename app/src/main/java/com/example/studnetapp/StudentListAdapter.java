package com.example.studnetapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {

    private ArrayList<StudentData> usersList;

    public StudentListAdapter(ArrayList<StudentData> usersList) {
        this.usersList = usersList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTxt;
        private TextView idTxt;
        private CheckBox checkBox;

        public MyViewHolder(final View view) {
            super(view);

            LinearLayout row = (LinearLayout)view.findViewById(R.id.student_row);
            row.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {

                    int pos = getAdapterPosition();

                    if(pos != RecyclerView.NO_POSITION){
                        goToViewStudentPage(v,pos);
                    }
                }
            });
            nameTxt = view.findViewById(R.id.name);
            idTxt = view.findViewById(R.id.id);
            checkBox = view.findViewById(R.id.checkBox);
        }
    }

    private void goToViewStudentPage(View v, int index) {
        Intent intent = new Intent(v.getContext(), viewStudentDetails.class);
        Bundle bundle = new Bundle();
        StudentData studentData = usersList.get(index);
        bundle.putString("name", studentData.name);
        bundle.putString("id", studentData.id);
        bundle.putString("pictureUlr", studentData.pictureUlr);
        bundle.putString("phone", studentData.phone);
        bundle.putString("address", studentData.address);
        bundle.putBoolean("checkbox", studentData.checkbox);
        intent.putExtra("STUDENT_DETAILS", bundle);
        v.getContext().startActivity(intent);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_tile, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentListAdapter.MyViewHolder holder, int position) {
        String name = usersList.get(position).getName();
        String id = usersList.get(position).getId();
        boolean checkbox = usersList.get(position).isCheckbox();
        holder.nameTxt.setText(name);
        holder.idTxt.setText(id);
        holder.checkBox.setChecked(checkbox);
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }
}
